package com.erfan


counter = 0
String datasetRoot = "/home/erfan/Documents/programming/invertedIndex/dataset/"

@SuppressWarnings("GrMethodMayBeStatic")
void initialFiles(String datasetRoot) {

    new File(datasetRoot).eachFile(){
        file ->
            file.renameTo(new File(datasetRoot + (counter as String)))
            counter++
    }
}

int addFile(String fileName, String datasetRoot){
    def master = new File("/home/erfan/Documents/programming/invertedIndex/src/com/erfan/master.txt")
    def counter = master.text as int
    def f = new File(fileName)
    if (f.isFile()){
        f.renameTo(new File(datasetRoot + (counter as String)))
        counter++
        master.setText(counter as String)
        return 1
    } else {
        println"not a valid text file"
        return 0
    }
}

//int toXML(String databaseRoot){
//    def fw = new FileWriter("/home/erfan/Documents/programming/invertedIndex/src/com/erfan/data.xml")
//    def xml = new MarkupBuilder(fw)
//
//    xml.records {
//
//    }
//    xml.
//    fw.close()
//
//
//    return 0
//}

void createIndex(String datasetRoot){
    def invertedIndex = new HashMap<String, LinkedList>()
    new File(datasetRoot).eachFile(){
        file ->
            println(file.name)
            file.eachLine(){
                line ->
                    parseAndAdd(file.name as int, line, invertedIndex)
            }
    }
    def fw = new FileWriter("/home/erfan/Documents/programming/invertedIndex/result.txt")
    invertedIndex.each {k, v ->
        fw.write(k + " " + v + "\n")
    }
    fw.close()
//    println(invertedIndex)

}

String[] parseAndAdd(int file, String line, HashMap index){
    def list = line.split(/[^A-Za-z]+/)
    list.each {token ->
        if(index.containsKey(token)){
            if(index[token][-1] != file)
                index[token].push(file)
        } else {
            index[token] = [file] as LinkedList<Integer>
        }
    }
}

void test(){
    s = "salam2 bare bar \n khooby?"
    def a = s.split(/[^A-Za-z]+/)
    println(a)
}

println("Hello world!!!!")
//initialFiles(datasetRoot)\
//addFile("/home/erfan/Documents/programming/invertedIndex/dataset7", datasetRoot)
createIndex(datasetRoot)